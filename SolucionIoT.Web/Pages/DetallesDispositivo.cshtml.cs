using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SolucionIoT.BIZ;
using SolucionIoT.COMMON.Entidades;
using SolucionIoT.Tools;

namespace SolucionIoT.Web.Pages
{
    [Authorize]
    public class DetallesDispositivoModel : PageModel
    {
        [BindProperty]
        public Dictionary<string,string> Opciones { get; set; }
        [BindProperty]
        public Dispositivo Dispositivo { get; set; }
        [BindProperty]
        public List<Lectura> Lecturas { get; set; }
        public void OnGet(string id,string opcion)
        {
            
            Dispositivo = FactoryManager.DispositivoManager().BuscarPorId(id);

#if DEBUG
            int numero=0;
            Random r = new Random();
            for (int i = 0; i < 22; i++)
            {
                numero = numero + 1;
                FactoryManager.LecturaManager().Insertar(new Lectura()
                {
                    IdDispositivo = id,
                    Personas = numero,
                    Contaminacion = r.Next(100, 600),
                    Luminosidad = r.Next(800, 1024)
                });
            }
#endif
            Lecturas = FactoryManager.LecturaManager().LecturasDelDispositivo(id).ToList();

            if (opcion != null)
            {
                MqttService.Publicar("PlataformaLEIKobra/" + id, opcion);
            }

            Opciones = new Dictionary<string, string>
            {
                { "R11", $"Encender {Dispositivo.UsoRelevador1}" },
                { "R10", $"Apagar {Dispositivo.UsoRelevador1}" },
                { "R21", $"Encender {Dispositivo.UsoRelevador2}" },
                { "R20", $"Apagar {Dispositivo.UsoRelevador2}" }
                //                { "R31", $"Encender {Dispositivo.UsoRelevador3}" },
                //{ "R30", $"Apagar {Dispositivo.UsoRelevador3}" },
            };
        }
    }
}
