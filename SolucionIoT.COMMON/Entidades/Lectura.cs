﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SolucionIoT.COMMON.Entidades
{
    public class Lectura:BaseDTO
    {
        public string IdDispositivo { get; set; }

        public float Personas { get; set; }

        public float Contaminacion { get; set; }

        public float Luminosidad { get; set; }
    }
}
