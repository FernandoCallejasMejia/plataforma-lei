﻿using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using SolucionIoT.BIZ.API;
using SolucionIoT.COMMON.Entidades;
using SolucionIoT.COMMON.Modelos;
using SolucionIoT.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SolucionIoT.ClienteWPF
{
    /// <summary>
    /// Lógica de interacción para PanelUsuario.xaml
    /// </summary>
    public partial class PanelUsuario : Window
    {
        readonly DispatcherTimer timer;
        private List<ComandosModel> opciones;
        readonly PanelUsuarioModel model;
        readonly Random r;
        private string mensaje;
        private MensajeRecibido mensajeRecibido;
        string topico;
        public PanelUsuario(Usuario usuario )
        {
            InitializeComponent();

            this.SizeChanged += PanelUsuario_SizeChanged;
            timer = new DispatcherTimer
            {
                Interval = new TimeSpan(0, 0, 0, 0, 100)
            };
            timer.Start();
            timer.Tick += Timer_Tick;
            r = new Random();
            MqttService.Conectar("PlataformaLEIWPF" + r.Next(0, 100000).ToString(), "broker.hivemq.com");
            MqttService.Conectado += MqttService_Conectado;
            MqttService.MensajeRecibido += MqttService_MensajeRecibido;
            MqttService.Error += MqttService_Error;
            MqttService.Mensaje += MqttService_Mensaje;
            model = this.DataContext as PanelUsuarioModel;
            model.Usuario = usuario;
            model.Dispositivos = FactoryManager.DispositivoManager().DispositivosDeUsuarioPorId(usuario.Id).ToList();
            lstDispositivos.ItemsSource = null;
            lstDispositivos.ItemsSource = model.Dispositivos;
            topico = "";
        }

        private void MqttService_Mensaje(object sender, string e)
        {
            mensaje = e;
        }

        private void MqttService_Error(object sender, string e)
        {
            mensaje = "Error: " + e;
        }

        private void MqttService_MensajeRecibido(object sender, MensajeRecibido e)
        {
            mensajeRecibido = e;
        }

        private void MqttService_Conectado(object sender, string e)
        {
            mensaje = e;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (mensaje != "")
            {
                lstMensajes.Items.Add($"{DateTime.Now.ToShortTimeString()}: {mensaje}");
                mensaje = "";
            }
            if (mensajeRecibido != null)
            {
                if (mensajeRecibido.Topico == topico)
                {
                    lstMensajes.Items.Add($"[{mensajeRecibido.Topico}]>{mensajeRecibido.Mensaje}");
                    if (mensajeRecibido.Mensaje.Contains("="))
                    {
                        string[] parte = mensajeRecibido.Mensaje.Split('=');
                        switch (parte[0])
                        {
                            case "R1":
                                lblER1.Content = parte[1] == "1" ? "Encendido" : "Apagado";
                                break;
                            case "R2":
                                lblER2.Content = parte[1] == "1" ? "Encendido" : "Apagado";
                                break;
                            //case "R3":
                            //    lblER3.Content = parte[1] == "1" ? "Encendido" : "Apagado";
                            //    break;
                            case "R":
                                lblER1.Content = parte[1][0] == '1' ? "Encendido" : "Apagado";
                                lblER2.Content = parte[1][1] == '1' ? "Encendido" : "Apagado";
                                //lblER3.Content = parte[1][2] == '1' ? "Encendido" : "Apagado";
                                break;
                            default:
                                break;
                        }
                    }
                }
                mensajeRecibido = null;
            }
        }

        private void PanelUsuario_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            plotGrafica.Width = e.NewSize.Width / 2 * .95;
        }

        private void lstDispositivos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ActualizarDatos();
            topico = $"PlataformaLEIKobra/{model.DispositivoSeleccionado.Id}";
            MqttService.Suscribir(topico);
            btnActualizar.IsEnabled = true;
        }

        private void ActualizarDatos()
        {
            this.Cursor = Cursors.Wait;
            model.DispositivoSeleccionado = lstDispositivos.SelectedItem as Dispositivo;

            model.LecturasDelDispositivo = FactoryManager.LecturaManager().LecturasDelDispositivo(model.DispositivoSeleccionado.Id).OrderBy(e => e.FechaHora).ToList();
            Graficar();
            dtgLecturas.ItemsSource = null;
            dtgLecturas.ItemsSource = model.LecturasDelDispositivo;
            lstMensajes.Items.Add($"{DateTime.Now.ToShortTimeString()}: Mostrando datos del dispositivo {model.DispositivoSeleccionado.Ubicacion}");
            opciones = new List<ComandosModel>()
            {
                new ComandosModel("R11",$"Encender " +model.DispositivoSeleccionado.UsoRelevador1 ),
                new ComandosModel("R10",$"Apagar " +model.DispositivoSeleccionado.UsoRelevador1 ),
                new ComandosModel("R21",$"Encender " +model.DispositivoSeleccionado.UsoRelevador2 ),
                new ComandosModel("R20",$"Apagar " +model.DispositivoSeleccionado.UsoRelevador2 )
                //new ComandosModel("R31",$"Encender " +model.DispositivoSeleccionado.UsoRelevador3 ),
                //new ComandosModel("R30",$"Apagar " +model.DispositivoSeleccionado.UsoRelevador3 ),
            };
            lblR1.Content = "Estado de " + model.DispositivoSeleccionado.UsoRelevador1;
            lblR2.Content = "Estado de " + model.DispositivoSeleccionado.UsoRelevador2;
            //lblR3.Content = "Estado de " + model.DispositivoSeleccionado.UsoRelevador3;
            lblER1.Content = "";
            lblER2.Content = "";
            //lblER3.Content = "";
            cmbOpciones.ItemsSource = null;
            cmbOpciones.ItemsSource = opciones;
            this.Cursor = Cursors.Arrow;
        }

        private void Graficar()
        {
            PlotModel grafica = new PlotModel();
            DateTimeAxis ejeTiempo = new DateTimeAxis();
            LineSeries personas = new LineSeries();
            LineSeries contaminacion = new LineSeries();
            LineSeries luminosidad = new LineSeries();
            foreach (var item in model.LecturasDelDispositivo)
            {
                personas.Points.Add(DateTimeAxis.CreateDataPoint(item.FechaHora, item.Personas));
                contaminacion.Points.Add(DateTimeAxis.CreateDataPoint(item.FechaHora, item.Contaminacion));
                luminosidad.Points.Add(DateTimeAxis.CreateDataPoint(item.FechaHora, item.Luminosidad));
            }
            personas.Title = "Personas";
            contaminacion.Title = "Contaminacion";
            luminosidad.Title = "Luminosidad";
            grafica.Axes.Add(ejeTiempo);
            grafica.Series.Add(personas);
            grafica.Series.Add(contaminacion);
            grafica.Series.Add(luminosidad);
            plotGrafica.Model = grafica;
        }

        private void cmbOpciones_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbOpciones.SelectedItem != null)
            {
                MqttService.Publicar(topico, (cmbOpciones.SelectedItem as ComandosModel).Comando);
            }
        }

        private void btnActualizar_Click(object sender, RoutedEventArgs e)
        {
            MqttService.Publicar(topico, "?R");
            Task.Delay(2500);
            MqttService.Publicar(topico, "?B");
        }

        
    }
}
